﻿using SegundoExamen.Interfaces;
using SegundoExamen.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoExamen.models
{
    [Serializable]
    public class List:File<List>
    {
        public string NombreLista { set; get;}
        List<List> Items { set; get; }


        public List FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<List>(filepath);
        }

        public List ReadObjectFromXml(string filePath)
        {
            throw new NotImplementedException();
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }

        public void WriteObjectInXml(string filePath)
        {
            throw new NotImplementedException();
        }
    }
}
