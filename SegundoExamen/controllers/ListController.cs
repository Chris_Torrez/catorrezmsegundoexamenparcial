﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SegundoExamen.models;
using System.Windows.Controls;
using System.Windows;

namespace SegundoExamen.controllers
{
    class ListController
    {

        private MainWindow window;
        private SaveFileDialog sfdialog;
        private OpenFileDialog ofdialog;

        public ListController(MainWindow window)
        {
            this.window = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }

        public void MainControllerEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "BtnSave":
                    Save();
                    break;
                case "BtnOpen":
                    Open();
                    break;
                case "BtnClear":
                   Clear();
                    break;
            }
        }

        private void Save()
        {
            sfdialog.Filter = "Xml File (*.xml)|*.xml";
            if (sfdialog.ShowDialog() == true)
            {
                List p = new List();
                p.NombreLista = window.listName.Text;
               
                // -------
                p.ToXml(sfdialog.FileName);
                Clear();
            }
        }

        private void Open()
        {
            ofdialog.Filter = "Xml File (*.xml)|*.xml";
            if (ofdialog.ShowDialog() == true)
            {
                List x = new List();
                // -------
                x = x.FromXml(ofdialog.FileName);
                window.listName.Text = x.NombreLista;
                
            }
        }

        public void Clear()
        {
            window.listName.Text = "";
            window.itemsTextBox.Text = "";
            
        }

    }
}
